#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/Pose.h>
#include <sstream>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <sensor_msgs/LaserScan.h>
#include <ros/time.h>
#include <tf/tf.h>
#include <nav_msgs/Odometry.h>


using namespace std;
using namespace geometry_msgs;

const float THRESHOLD_X=0.1f;
const float THRESHOLD_Z=1.0f;
const float ISTERESI =0.3f;

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;
void sendNewGoal();
ros::Publisher cmd_vel_pub;

ros::Time tag_last_msg;
ros::Duration tag_timeout(5);

ros::Time follow_last_msg;
ros::Duration follow_timeout(30);

double _max_tv,_max_rv,kt,kr, male;
geometry_msgs::Twist cmd_vel;
MoveBaseClient* ac;
std::vector<geometry_msgs::Pose2D> pose, work;
int next_pose,work_pose;

void initWayPoints(){
    geometry_msgs::Pose2D primo;
    primo.x=1.5;
    primo.y=0;
    primo.theta=0;
    pose.push_back(primo);
    primo.x=1.5;
    primo.y=1.5;
    primo.theta=1.57;
    pose.push_back(primo);
    primo.x=0;
    primo.y=1.5;
    primo.theta=3.14;
    pose.push_back(primo);
    primo.x=0;
    primo.y=0;
    primo.theta=-1.57;
    pose.push_back(primo);
    next_pose=0;

}
float zmax=-100;

void followTag(const geometry_msgs::Pose::ConstPtr& tag){    
    ROS_INFO("I heard: [%f]", tag->position.x);
/*	int X=640;
    float tv = 0;
    float rv = 0;
    float sign=1;
    float THRESHOLD_DISTANCE = 0.7; //distanza dal tag
    float THRESHOLD_ANGLE = 50; //distanza dal tag
    
		if(tag->position.z != tag->position.z)
			tv=0;
		else
			tv=sign*kt*(tag->position.z-THRESHOLD_DISTANCE);
		if(tag->position.x>=640/2+THRESHOLD_ANGLE)
			rv=-0.5;
		else if(tag->position.x<=640/2-THRESHOLD_ANGLE)
			rv=0.5;
		else rv=0;
		if (fabs(rv)>_max_rv){
			float scale=_max_rv/fabs(rv);
			tv*=scale;
			rv*=scale;
		}

		if (fabs(tv)>_max_tv){
			float scale=_max_tv/fabs(tv);
			tv*=scale;
			rv*=scale;
		}
		if(fabs(tv)<0.05){
			tv=0;
		}
		if(fabs(rv)<0.01){
			rv=0;
		}
	
    cmd_vel.linear.x = tv;
    cmd_vel.angular.z = rv;
    cmd_vel_pub.publish(cmd_vel);*/

    float ang_vel= 0.0f;
    float lin_vel=0.0f;

    if(tag->position.x >= THRESHOLD_X)
        {
            ang_vel=-0.2;
        }
    else if( tag->position.x <= -THRESHOLD_X)
    {
            ang_vel=0.2;
    }
    else if(tag->position.z >=THRESHOLD_Z)
    {
        lin_vel=0.15f;
    }
    else if(tag->position.z <= (THRESHOLD_Z - ISTERESI))
    {
        lin_vel=-0.15f;
    }
    
    cmd_vel.linear.x = lin_vel;
    cmd_vel.angular.z = ang_vel;
    cmd_vel_pub.publish(cmd_vel);

}


void doneCb(const actionlib::SimpleClientGoalState &state, const move_base_msgs::MoveBaseResultConstPtr &result)
{
    if(state.state_ == actionlib::SimpleClientGoalState::SUCCEEDED){
        ROS_INFO("The goal was reached!");
        next_pose=(int)(next_pose+1)%((int)pose.size());
        sendNewGoal();
    }
    if(state.state_ == actionlib::SimpleClientGoalState::ABORTED)
        ROS_WARN("Failed to reach the goal...");

} 


void sendNewGoal()
{
	move_base_msgs::MoveBaseGoal goal;

        //we'll send a goal to the robot to move 1 meter forward
        goal.target_pose.header.frame_id = "odom";
        goal.target_pose.header.stamp = ros::Time::now();

        goal.target_pose.pose.position.x = pose[next_pose].x;
        goal.target_pose.pose.position.y = pose[next_pose].y;

        goal.target_pose.pose.orientation = tf::createQuaternionMsgFromYaw(pose[next_pose].theta);

        ROS_INFO("Sending goal");
        ac->sendGoal(goal,&doneCb);
}

void faceCallback(const geometry_msgs::Pose::ConstPtr& msg)
{   ros::Time tag_new_msg = ros::Time::now();
    ros::Time follow_new_msg = ros::Time::now();
	int status;
	float z=msg->position.z;
	if(z!=z) status=0;
	else status=1;
    switch(status)
    {	case (0):
            ROS_INFO("STOP");
            ac->cancelAllGoals();
            cmd_vel.linear.x = 0;
            cmd_vel.angular.z = 0;
            cmd_vel_pub.publish(cmd_vel);

            break;
        case (1):
            ROS_INFO("FOLLOW");
            ac->cancelAllGoals();
            followTag(msg);
            if(follow_new_msg-follow_last_msg > follow_timeout) {
            follow_last_msg = follow_new_msg;
		}
            break;
        default:
            cmd_vel.linear.x = 0;
            cmd_vel.angular.z = 0;
            cmd_vel_pub.publish(cmd_vel);
    }
}


int main(int argc, char** argv){
  ros::init(argc, argv, "Tracer");
	_max_rv=0.5;
	_max_tv=0.15;
	kt=1.0;
	kr=-2.0;
	male=0.4;
	initWayPoints();
	
    ros::NodeHandle n;
    ros::NodeHandle nh("~");
    nh.param("max_rv", _max_rv, 1.0);
    nh.param("max_tv", _max_tv, 0.3);
    nh.param("kt", kt, 2.0);
    nh.param("kr", kr, -4.0);
    nh.param("male", male, 0.4); //distanza di arresto del robot per obstacle avoidance
    initWayPoints();

    tag_last_msg = ros::Time::now();
    follow_last_msg = ros::Time::now();

    cmd_vel_pub = n.advertise<geometry_msgs::Twist>("/cmd_vel", 1000);
    ros::Subscriber sub = n.subscribe("/OpenFace/Face_tracer",1000,faceCallback);
    ac=new MoveBaseClient("move_base", true);

 
    ros::spin();


  return 0;
}

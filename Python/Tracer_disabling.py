import socket
import sys
from subprocess import call

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind the socket to the address given on the command line
#server_address = ('192.168.1.2', 9000)
#sock.bind(server_address)
#print >>sys.stderr, 'starting up ROS server on %s port %s' % sock.getsockname()
#sock.listen(1)

while True:
    sock.connect(('192.168.1.1', 9000))
    try:
        print >> sys.stderr, 'connected to server'
        while True:
            BUFFER_SIZE = 1024
	    print >>sys.stderr, 'before received"'		
            data = sock.recv(BUFFER_SIZE)
	    print "after received"
	    print >>sys.stderr, 'received "%s"' % data		
            if data.startswith("disable"):
                print >>sys.stderr, 'disabling tracking "%s"' % data

		call(["rosnode", "kill", "Tracer"])
		call(["rosnode", "kill", "Audio"])
		
        
    finally:
        connection.close()

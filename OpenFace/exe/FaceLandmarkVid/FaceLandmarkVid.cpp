// FaceTrackingVid.cpp : Defines the entry point for the console application for tracking faces in videos.

// Libraries for landmark detection (includes CLNF and CLM modules)

//ROS includes
#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <cv_bridge/cv_bridge.h>
#include <std_msgs/String.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/Pose.h>

#include "LandmarkCoreIncludes.h"
#include "GazeEstimation.h"

#include <fstream>
#include <sstream>

// OpenCV includes
#include <opencv2/videoio/videoio.hpp>  // Video write
#include <opencv2/videoio/videoio_c.h>  // Video write
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

//OpenNI includes
#include <opencv2/OpenNI/OpenNI.h>

// Boost includes
#include <filesystem.hpp>
#include <filesystem/fstream.hpp>


#define INFO_STREAM( stream ) \
std::cout << stream << std::endl

#define WARN_STREAM( stream ) \
std::cout << "Warning: " << stream << std::endl

#define ERROR_STREAM( stream ) \
std::cout << "Error: " << stream << std::endl

static void printErrorAndAbort( const std::string & error )
{
    std::cout << error << std::endl;
    abort();
}

#define FATAL_STREAM( stream ) \
printErrorAndAbort( std::string( "Fatal error: " ) + stream )

using namespace std;
using namespace cv;
using namespace cv_bridge;
using namespace sensor_msgs;
using namespace message_filters;
using namespace geometry_msgs;


vector<string> files, depth_directories, output_video_files, out_dummy;

string output_codec;

LandmarkDetector::CLNF *clnf_model;

LandmarkDetector::FaceModelParameters *det_parameters;

float fx = 0, fy = 0, cx = 0, cy = 0;

bool cx_undefined = false;
bool fx_undefined = false;

int f_n = -1;

bool done = false;

cv::Vec6d pose_estimate_to_draw;
cv::Vec6d last_pose_estimate(0,0,0,0,0,0);
geometry_msgs::Pose head_pose;
ros::Publisher pos_pub;



vector<string> get_arguments(int argc, char **argv)
{

	vector<string> arguments;

	for(int i = 0; i < argc; ++i)
	{
		arguments.push_back(string(argv[i]));
	}
	return arguments;
}

// Some globals for tracking timing information for visualisation
double fps_tracker = -1.0;
int64 t0 = 0;

// Visualising the results
void visualise_tracking(cv::Mat& captured_image, cv::Mat& depth_image, const LandmarkDetector::CLNF& face_model, const LandmarkDetector::FaceModelParameters& det_parameters, cv::Point3f gazeDirection0, cv::Point3f gazeDirection1, int frame_count,cv::Vec6d &pose_h, double fx, double fy, double cx, double cy)
{

	// Drawing the facial landmarks on the face and the bounding box around it if tracking is successful and initialised
	double detection_certainty = face_model.detection_certainty;
	bool detection_success = face_model.detection_success;

	double visualisation_boundary = 0.2;

	// Only draw if the reliability is reasonable, the value is slightly ad-hoc
	if (detection_certainty < visualisation_boundary)
	{
		LandmarkDetector::Draw(captured_image, face_model);

		double vis_certainty = detection_certainty;
		if (vis_certainty > 1)
			vis_certainty = 1;
		if (vis_certainty < -1)
			vis_certainty = -1;

		vis_certainty = (vis_certainty + 1) / (visualisation_boundary + 1);

		// A rough heuristic for box around the face width
		int thickness = (int)std::ceil(2.0* ((double)captured_image.cols) / 640.0);

		pose_h = LandmarkDetector::GetCorrectedPoseCamera(face_model, fx, fy, cx, cy);

		// Draw it in reddish if uncertain, blueish if certain
		LandmarkDetector::DrawBox(captured_image, pose_h, cv::Scalar((1 - vis_certainty)*255.0, 0, vis_certainty * 255), thickness, fx, fy, cx, cy);

		if (det_parameters.track_gaze && detection_success && face_model.eye_model)
		{
			FaceAnalysis::DrawGaze(captured_image, face_model, gazeDirection0, gazeDirection1, fx, fy, cx, cy);
		}
	}

	// Work out the framerate
	if (frame_count % 10 == 0)
	{
		double t1 = cv::getTickCount();
		fps_tracker = 10.0 / (double(t1 - t0) / cv::getTickFrequency());
		t0 = t1;
	}

	// Write out the framerate on the image before displaying it
	char fpsC[255];
	std::sprintf(fpsC, "%d", (int)fps_tracker);
	string fpsSt("FPS:");
	fpsSt += fpsC;

	cv::putText(captured_image, fpsSt, cv::Point(10, 20), CV_FONT_HERSHEY_SIMPLEX, 0.5, CV_RGB(255, 0, 0));

	if (!det_parameters.quiet_mode)
	{
		cv::namedWindow("tracking_result", 1);
		cv::imshow("tracking_result", captured_image);
	

		if (!depth_image.empty())
		{
			// Division needed for visualisation purposes
			imshow("depth", depth_image);
		}

	}
}


void callback(const ImageConstPtr& image_rgb, const ImageConstPtr& image_depth)
{
		string current_file;

		// We might specify multiple video files as arguments
		if(files.size() > 0)
		{
			f_n++;
		    current_file = files[f_n];
		}
		else
		{
			// If we want to write out from webcam
			f_n = 0;
		}

		bool use_depth = !depth_directories.empty();

		cv_bridge::CvImagePtr image_asus_1;
		cv_bridge::CvImagePtr depth_asus_1;

		 try{
       		image_asus_1 = cv_bridge::toCvCopy(image_rgb, sensor_msgs::image_encodings::BGR8);
    		}
    	catch (cv_bridge::Exception& e)
    	{
       		ROS_ERROR("cv_bridge exception:  %s", e.what());
       	 	exit (0);
   		 }
   		try{
        	depth_asus_1 = cv_bridge::toCvCopy(image_depth, sensor_msgs::image_encodings::TYPE_16UC1);
   		 }
    	catch (cv_bridge::Exception& e)
    	{
        	ROS_ERROR("cv_bridge exception:  %s", e.what());
        	exit (0);
    	}

		cv::Mat image_asus = image_asus_1->image;
		cv::Mat depth_asus = depth_asus_1->image;

		// If optical centers are not defined just use center of image
		if (cx_undefined)
		{
			cx = image_asus.cols / 2.0f;
			cy = image_asus.rows / 2.0f;
		}
		// Use a rough guess-timate of focal length
		if (fx_undefined)
		{
			fx = 500 * (image_asus.cols / 640.0);
			fy = 500 * (image_asus.rows / 480.0);

			fx = (fx + fy) / 2.0;
			fy = fx;
		}

		int frame_count = 0;

		// saving the videos
		cv::VideoWriter writerFace;
		if (!output_video_files.empty())
		{
			try
 			{
				writerFace = cv::VideoWriter(output_video_files[f_n], CV_FOURCC(output_codec[0], output_codec[1], output_codec[2], output_codec[3]), 30, image_asus.size(), true);
			}
			catch(cv::Exception e)
			{
				WARN_STREAM( "Could not open VideoWriter, OUTPUT FILE WILL NOT BE WRITTEN. Currently using codec " << output_codec << ", try using an other one (-oc option)");
			}
		}

		// Use for timestamping if using a webcam
		int64 t_initial = cv::getTickCount();
	
			cv:Mat captured_image;

			float scaleFactor = 1.0f;
			image_asus.convertTo( captured_image, CV_8UC3, scaleFactor );

			// Reading the images
			cv::Mat_<float> depth_image;
			cv::Mat_<uchar> grayscale_image;

			if(captured_image.channels() == 3)
			{
				cv::cvtColor(captured_image, grayscale_image, CV_BGR2GRAY);
			}
			else
			{
				grayscale_image = captured_image.clone();
			}

			// Get depth image
			if(use_depth)
			{
			
				float scaleFactor_depth = 0.5f;
				depth_asus.convertTo( depth_image, CV_32FC1, scaleFactor_depth );


			}

			// The actual facial landmark detection / tracking
			bool detection_success = LandmarkDetector::DetectLandmarksInVideo(grayscale_image, depth_image, *clnf_model, *det_parameters);


			// Visualising the results
			// Drawing the facial landmarks on the face and the bounding box around it if tracking is successful and initialised
			double detection_certainty = clnf_model->detection_certainty;

			// Gaze tracking, absolute gaze direction
			cv::Point3f gazeDirection0(0, 0, -1);
			cv::Point3f gazeDirection1(0, 0, -1);

			if (det_parameters->track_gaze && detection_success && clnf_model->eye_model)
			{
				FaceAnalysis::EstimateGaze(*clnf_model, gazeDirection0, fx, fy, cx, cy, true);
				FaceAnalysis::EstimateGaze(*clnf_model, gazeDirection1, fx, fy, cx, cy, false);
			}


			visualise_tracking(captured_image, depth_image, *clnf_model, *det_parameters, gazeDirection0, gazeDirection1, frame_count, pose_estimate_to_draw, fx, fy, cx, cy);

			cout<<"DETECTION: "<<detection_success<<endl;

			// output the tracked video
			if (!output_video_files.empty())
			{
				writerFace << captured_image;
			}


		//	video_capture >> captured_image;

			// detect key presses
			char character_press = cv::waitKey(1);

			// restart the tracker
			if(character_press == 'r')
			{
				clnf_model->Reset();
			}
			// quit the application
			else if(character_press=='q')
			{
				exit (0);
			}
			else if(character_press=='p'){
				cout<<"X: "<<pose_estimate_to_draw[0]<<endl;
				cout<<"Y: "<<pose_estimate_to_draw[1]<<endl;
				cout<<"Z: "<<pose_estimate_to_draw[2]<<endl;
			}

			head_pose.position.x=pose_estimate_to_draw[0]/1000;
			head_pose.position.y=pose_estimate_to_draw[1]/1000;
			head_pose.position.z=pose_estimate_to_draw[2]/1000;

			if(detection_success){
							pos_pub.publish(head_pose);
			 }

			// Update the frame count
			frame_count++;

}




int main (int argc, char **argv)
{

	vector<string> arguments;
	arguments.push_back(string("-inroot"));
	arguments.push_back(string("~/OpenFace/exe"));
	arguments.push_back(string("-fd"));
	arguments.push_back(string("FaceLandmarkVid"));

	arguments.push_back(string("-mloc"));
	arguments.push_back(string("/home/mario/Scrivania/OpenFace/lib/local/LandmarkDetector/model/main_clnf_general.txt"));

	cout<<"INFORMATION: "<<endl;
	cout<<cv::getBuildInformation()<<endl;

	// By default try webcam 0
	int device; // = 910;


	det_parameters = new LandmarkDetector::FaceModelParameters(arguments);
	cout<<det_parameters->model_location<<endl;

	// Get the input output file parameters

	// Indicates that rotation should be with respect to world or camera coordinates
	bool u;
	LandmarkDetector::get_video_input_output_params(files, depth_directories, out_dummy, output_video_files, u, output_codec, arguments);

	// The modules that are being used for tracking
	clnf_model= new LandmarkDetector::CLNF(det_parameters->model_location);

	// Get camera parameters
	LandmarkDetector::get_camera_params(device, fx, fy, cx, cy, arguments);

	// If cx (optical axis centre) is undefined will use the image size/2 as an estimate

	if (cx == 0 || cy == 0)
	{
		cx_undefined = true;
	}

	if (fx == 0 || fy == 0)
	{
		fx_undefined = true;
	}

	// If multiple video files are tracked, use this to indicate if we are done

	det_parameters->track_gaze = true;

	ros::init(argc, argv, "OpenFace");

	ros::NodeHandle nh("~");
	pos_pub = nh.advertise<geometry_msgs::Pose>("Face_tracer", 1000);

	message_filters::Subscriber<Image> rgb_sub(nh, "/camera/rgb/image_raw", 1);
	message_filters::Subscriber<Image> depth_sub(nh, "/camera/depth/image_raw", 1);

	typedef sync_policies::ApproximateTime<Image, Image> MySyncPolicy;

  // ApproximateTime takes a queue size as its constructor argument, hence MySyncPolicy(10)
	Synchronizer<MySyncPolicy> sync(MySyncPolicy(10), rgb_sub, depth_sub);
	sync.registerCallback(boost::bind(&callback, _1, _2));

	ros::spin();

	return 0;

}

